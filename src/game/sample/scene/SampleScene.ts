import { 
    FreeCamera, 
    Vector3, 
    HemisphericLight, 
    MeshBuilder, 
    Mesh, 
    Scene, 
    KeyboardEventTypes
} from '@babylonjs/core';
import {
    AdvancedDynamicTexture,
    TextBlock
} from '@babylonjs/gui';
import { setupCameraAutoAdjust } from '../../util/CameraAdjustment';
import { GameScene } from '../../interface';
import GameInstanceManager from '../../manager/GameInstanceManager';
import SampleScene2 from './SampleScene2';

export default class SampleScene implements GameScene {
    private box?: Mesh;
    
    /**
     * Scene create function
     * @param scene Scene object that has been registered to Babylon engine
     */
    public onSceneReady = (scene: Scene) => {
        // This creates and positions a free camera (non-mesh)
        var camera = new FreeCamera("camera1", new Vector3(0, 5, -10), scene);

        // This targets the camera to scene origin
        camera.setTarget(Vector3.Zero());

        const canvas = scene.getEngine().getRenderingCanvas();

        // This attaches the camera to the canvas
        camera.attachControl(canvas as HTMLElement, true);

        // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
        var light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);

        // Default intensity is 1. Let's dim the light a small amount
        light.intensity = 0.7;

        // Our built-in 'box' shape.
        this.box = MeshBuilder.CreateBox("box", {size: 2}, scene);

        // Move the box upward 1/2 its height
        this.box.position.y = 1;

        // Our built-in 'ground' shape.
        MeshBuilder.CreateGround("ground", {width: 6, height: 6}, scene);

        /**
         * Sets auto-adjust on camera to ensure that all objects have the same size with 
         * respect to screen size
         */
        setupCameraAutoAdjust(scene, camera);
        this.setupControl(scene);
        this.setupGUI(scene);
    }

    setupControl(scene: Scene) {
        scene.onKeyboardObservable.add((kbInfo) => {
            switch (kbInfo.type) {
                case KeyboardEventTypes.KEYDOWN:
                    console.log("KEY DOWN: ", kbInfo.event.key);
                    break;
                case KeyboardEventTypes.KEYUP:
                    const keyCode = kbInfo.event.keyCode;
                    console.log("KEY UP: ", keyCode);
                    switch (keyCode) {
                        case 82:
                            GameInstanceManager.Instance?.startScene(new SampleScene2());
                            break;
                    }
                    break;
            }
        });
    }

    setupGUI(scene: Scene) {
        var advancedTexture = AdvancedDynamicTexture.CreateFullscreenUI("UI", true, scene);
        /**
         * Ideal width is used for texture size reference.
         * E.g. if set to 720, then when the screen's width is only 360 all textures size would be halved
         */
        advancedTexture.idealWidth = 720;
        advancedTexture.renderAtIdealSize = true;

        var text = new TextBlock();
        text.text = "Press R to go to SampleScene2";
        text.color = "white";
        text.fontSize = 24;
        advancedTexture.addControl(text);    
    }

    /**
     * Scene's update function
     */
    public onRender = (scene: Scene) => {
        if (this.box !== undefined) {
            var deltaTimeInMillis = scene.getEngine().getDeltaTime();

            const rpm = 10;
            this.box.rotation.y += ((rpm / 60) * Math.PI * 2 * (deltaTimeInMillis / 1000));
        }
    }
}