import { Engine, EngineOptions, Nullable, Scene } from '@babylonjs/core';
import { GameScene } from '../interface';

/**
 * Property used for Babylon's engine
 */
export type GameInstanceProps = {
    /**
     * The canvas on which Babylon is rendered
     */
    canvas: Nullable<HTMLCanvasElement>;
    /**
     * Anti-aliasing on objects
     */
    antialias?: boolean;
    /**
     * Optional Babylon engine's options
     */
    engineOptions?: EngineOptions;
    /**
     * Set true to enhance texture quality on high-DPI devices
     */
    adaptToDeviceRatio?: boolean;
};

export default class GameInstanceManager {
    private static instance?: GameInstanceManager;

    /**
     * Gets the static reference to GameInstanceManager
     */
    static get Instance() {
        return this.instance;
    }

    /**
     * The game's engine
     */
    engine: Engine;

    /**
     * The current active scene (currently only allows 1 scene to run at a time)
     */
    currentScene: Scene | undefined;

    constructor(props: GameInstanceProps) {
        this.engine = new Engine(
            props.canvas, 
            props.antialias, 
            props.engineOptions, 
            props.adaptToDeviceRatio
        );

        if (window) {
            window.addEventListener('resize', () => {
                if (this.engine) this.engine.resize();
            });
        }

        GameInstanceManager.instance = this;
    }

    /**
     * Shuts the current scene and starts a new one
     * @param config Config for the new scene
     */
    public startScene(config: GameScene) {
        if (this.currentScene) this.currentScene.dispose();

        /**
         * Scene initialization
         */
        const scene = new Scene(this.engine, config.sceneOptions);
        if (scene.isReady()) {
            config.onSceneReady(scene);
        } else {
            scene.onReadyObservable.addOnce((scene: Scene) => config.onSceneReady(scene));
        }

        /**
         * Registers scene update function to engine
         */
        this.engine.runRenderLoop(() => {
            if (typeof config.onRender === 'function') {
                config.onRender(scene);
            }
            scene.render();
        })

        this.currentScene = scene;
    }
}